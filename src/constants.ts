import { resolve } from "node:path";
import type { CompilerOptions } from "typescript";

import pkgJson from "../package.json";

interface Config {
  package: string;
  packageVersion: string;
  tsConfig: string;
}

interface OutputConfig extends Config {
  compilerOptions: CompilerOptions;
}

type Configs = Config[];
export type OutputConfigs = OutputConfig[];

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toJSON
export type Data = {
  timestamp: Date;
  tsVersion: string;
  configs: OutputConfigs;
};

export const TS_VERSION: string = pkgJson.devDependencies.typescript;

export const CONFIGS: Configs = [
  {
    package: "@sindresorhus/tsconfig",
    packageVersion: pkgJson.dependencies["@sindresorhus/tsconfig"],
    tsConfig: resolve(
      __dirname,
      "../",
      "node_modules/@sindresorhus/tsconfig/tsconfig.json",
    ),
  },
  {
    package: "@vue/tsconfig",
    packageVersion: pkgJson.dependencies["@vue/tsconfig"],
    tsConfig: resolve(
      __dirname,
      "../",
      "node_modules/@vue/tsconfig/tsconfig.dom.json",
    ),
  },
  {
    package: "@octokit/tsconfig",
    packageVersion: pkgJson.dependencies["@octokit/tsconfig"],
    tsConfig: resolve(
      __dirname,
      "../",
      "node_modules/@octokit/tsconfig/tsconfig.json",
    ),
  },
  {
    package: "@grafana/tsconfig",
    packageVersion: pkgJson.dependencies["@grafana/tsconfig"],
    tsConfig: resolve(
      __dirname,
      "../",
      "node_modules/@grafana/tsconfig/tsconfig.json",
    ),
  },
  {
    package: "@spotify/tsconfig",
    packageVersion: pkgJson.dependencies["@spotify/tsconfig"],
    tsConfig: resolve(
      __dirname,
      "../",
      "node_modules/@spotify/tsconfig/tsconfig.json",
    ),
  },
];

export const OUTPUT_FILENAME: string = resolve(__dirname, "../data/data.json");
export const MIN_OUTPUT_FILENAME: string = resolve(
  __dirname,
  "../data/data.min.json",
);

// export const IGNORE_COMPILER_OPTIONS: (keyof CompilerOptions)[] = [
//   "configFilePath",
// ];

import { ensureDirSync, writeJsonSync } from "fs-extra";
import { basename, dirname } from "node:path";
import { parseJsonConfigFileContent, readConfigFile, sys } from "typescript";

import type { Data, OutputConfigs } from "./constants";
import {
  CONFIGS,
  MIN_OUTPUT_FILENAME,
  OUTPUT_FILENAME,
  TS_VERSION,
} from "./constants";

function main() {
  ensureDirSync(dirname(OUTPUT_FILENAME));

  const outputConfigs: OutputConfigs = [];
  for (const config of CONFIGS) {
    // console.log(config);
    const tsConfigFile = readConfigFile(config.tsConfig, sys.readFile);
    // console.log(tsConfigFile);

    const tsConfigContent = parseJsonConfigFileContent(
      tsConfigFile.config,
      sys,
      dirname(config.tsConfig),
      // "./",
    );
    // console.log(tsConfigContent);

    const compilerOptions = tsConfigContent.options;
    outputConfigs.push({
      ...config,
      tsConfig: basename(config.tsConfig),
      compilerOptions,
    });
    // console.log(compilerOptions);
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
    // console.log(JSON.stringify(compilerOptions, undefined, 2));
  }

  // console.log(outputConfigs);
  const outputData: Data = {
    timestamp: new Date(),
    tsVersion: TS_VERSION,
    configs: outputConfigs,
  };

  writeJsonSync(OUTPUT_FILENAME, outputData, { spaces: 2 });
  writeJsonSync(MIN_OUTPUT_FILENAME, outputData, { spaces: 0 });
}

main();

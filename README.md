# tsconfig-pkg-stats

Summary statistics about [TSConfig](https://www.typescriptlang.org/tsconfig) file packages.

## References

- @sindresorhus/tsconfig:
  - https://github.com/sindresorhus/tsconfig
- @vue/tsconfig
  - https://github.com/vuejs/tsconfig
- @octokit/tsconfig
  - https://github.com/octokit/tsconfig
- @grafana/tsconfig:
  - https://github.com/grafana/tsconfig-grafana
- @spotify/tsconfig:
  - https://github.com/spotify/web-scripts/tree/master/packages/tsconfig
  - https://www.npmjs.com/package/@spotify/tsconfig

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version
```

```bash
npm install
```

```bash
npm run dev
```

```bash
npm run format
```

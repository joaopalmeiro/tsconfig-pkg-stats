# Notes

- Use your email address and an access token to set up GitHub Desktop with Codeberg:
  - https://docs.codeberg.org/advanced/access-token/
- https://stackoverflow.com/a/53898219
- https://github.com/microsoft/TypeScript/wiki/Using-the-Compiler-API
- https://github.com/microsoft/TypeScript/wiki/API-Breaking-Changes
- https://github.com/unjs/pkg-types
- https://github.com/dominikg/tsconfck
- https://stackoverflow.com/a/70128401
- https://github.com/desktop/desktop/blob/development/docs/integrations/gitlab.md
- https://github.com/microsoft/TypeScript/blob/v5.2.2/src/compiler/commandLineParser.ts#L146
- https://github.com/microsoft/TypeScript/blob/v5.2.2/src/compiler/commandLineParser.ts#L131
- https://github.com/microsoft/TypeScript/blob/v5.2.2/src/compiler/commandLineParser.ts#L540
- https://github.com/instacart/Snacks
- https://instacart.github.io/Snacks/#svgicons
- https://github.com/instacart/Snacks/tree/master/src/components/SVGIcon/icons
- https://kongponents.konghq.com/components/icon.html
- https://github.com/Kong/kongponents/tree/main/src/components/KIcon/icons

## Commands

```bash
npm install @sindresorhus/tsconfig @vue/tsconfig @octokit/tsconfig @grafana/tsconfig @spotify/tsconfig
```

```bash
npm install -D prettier jiti typescript fs-extra @types/fs-extra
```

```bash
npm install -D "@types/node@$(cat .nvmrc | cut -d . -f 1-2)"
```

```bash
cat .nvmrc | cut -d . -f 1-2
```

```bash
rm -rf node_modules/ && npm install
```

## Snippets

```js
{
  package: "",
  packageVersion: pkgJson.dependencies[""],
  tsConfig: resolve(__dirname, "../", ""),
}
```
